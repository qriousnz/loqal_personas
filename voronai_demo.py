#! /usr/bin/env python3

"""
Voronoi areas rock!

https://en.wikipedia.org/wiki/File:Voronoi_growth_euclidean.gif
"""

from itertools import combinations, cycle
import unittest
from webbrowser import open_new_tab

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '../..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db, folium_map
GeomDets = folium_map.GeomDets

## https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.Voronoi.html
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import Voronoi
from shapely.geometry import Polygon

## Made using https://codepen.io/jhawes/pen/
NZ_POLYGON_COORDS_ROUGH = [(-34.25665, 172.59032), (-34.58559, 172.47926),
    (-34.97628, 172.80216), (-37.47158, 174.45319), (-38.70132, 174.38401),
    (-39.23753, 173.43604), (-39.97205, 174.02615), (-40.19352, 174.79617),
    (-40.71472, 174.51151), (-40.50912, 173.95219), (-40.63719, 173.48075),
    (-40.26423, 172.40801), (-41.30482, 171.67016), (-42.51115, 170.69699),
    (-43.18411, 169.8117), (-44.08694, 167.76187), (-45.84003, 165.84387),
    (-46.47597, 166.50363), (-46.62382, 167.42706), (-47.09811, 167.16454),
    (-47.47278, 167.38657), (-47.35791, 168.27008), (-46.85513, 168.43191),
    (-46.80072, 169.69237), (-46.08552, 170.93888), (-45.12518, 171.36646),
    (-44.20523, 171.92654), (-44.04065, 172.5197), (-44.14032, 172.90411),
    (-43.84808, 173.58482), (-43.39095, 173.18887), (-42.60775, 173.97944),
    (-41.94974, 174.41252), (-41.47427, 174.34341), (-41.44977, 174.52699),
    (-41.77053, 175.16331), (-41.72994, 176.08527), (-39.54413, 177.51583),
    (-39.39052, 178.14322), (-39.00644, 178.21031), (-37.58286, 178.8377),
    (-37.39059, 178.1022), (-37.79055, 177.45459), (-37.71141, 176.83503),
    (-37.4754, 176.33632), (-36.27901, 175.68503), (-35.98455, 175.44926),
    (-36.09477, 175.08968), (-35.92954, 174.80699), (-35.1844, 174.54418),
    (-34.86334, 173.7481), (-34.62101, 173.23196), (-34.24185, 173.07287),
    (-34.25665, 172.59032)]
NZ_POLYGON_COORDS_ROUGH.append(NZ_POLYGON_COORDS_ROUGH[0])

"""

a-b and b-c etc might all be within MAX_IMMEDIATE_NEIGHBOUR_DIST but a-f needs
to be within MAX_CLUSTER_WIDTH otherwise it is getting crazy and our assumption
there are reasonably tightly clustered towers (assumed to only ever be off
because of different levels of precision or very small errors) is false. Time to
repair underlying data in feed.

a -- b -- c -- d -- f
     |
     f
"""
MAX_CLUSTER_WIDTH = 1  ## 1 km is the biggest gap we ever want to see between two "towers" in a tower cluster
MAX_IMMEDIATE_NEIGHBOUR_DIST = 0.1  ## 100m (too close not enough for AWSXL/U pairing, for example, which are on the same tower

def _make_pair_dets_tbl(con_local, cur_local):
    db.Pg.drop_tbl(con_local, cur_local, tbl=conf.TOWER_PAIR_DETS)
    sql_make_tbl = """\
    SELECT
      a_tower,
      b_tower,
      dist_km
    INTO {tower_pair_dets}
    FROM (
      SELECT
          a.tower AS
        a_tower,
         b.tower AS
        b_tower,
          a.tower_lat AS
        a_lat,
          a.tower_lon AS
        a_lon,
          b.tower_lat AS
        b_lat,
          b.tower_lon AS
        b_lon,
          (ST_DISTANCE(
            ST_GeomFromText('POINT(' || a.tower_lon || ' ' || a.tower_lat || ')', {srid}),
            ST_GeomFromText('POINT(' || b.tower_lon || ' ' || b.tower_lat || ')', {srid})
          )*{scalar})::float
        dist_km
      FROM
      (
        SELECT DISTINCT ON (tower)
          tower, tower_lat, tower_lon
        FROM (
          SELECT DISTINCT ON (cell_id_rti)
            tower, tower_lat, tower_lon
            FROM {feed_rtis}
            WHERE tower_lat BETWEEN {min_lat} AND {max_lat}
              AND tower_lon BETWEEN  {min_lon} AND {max_lon}
            ORDER BY cell_id_rti, tower_lat, tower_lon
        ) AS src
        ORDER BY tower, tower_lat, tower_lon, description
      ) AS a

      CROSS JOIN

      (
        SELECT DISTINCT ON (tower)
          tower, tower_lat, tower_lon
        FROM (
          SELECT DISTINCT ON (cell_id_rti)
            tower, tower_lat, tower_lon
            FROM {feed_rtis}
            WHERE tower_lat BETWEEN {min_lat} AND {max_lat}
              AND tower_lon BETWEEN  {min_lon} AND {max_lon}
            ORDER BY cell_id_rti, tower_lat, tower_lon
        ) AS src
        ORDER BY tower, tower_lat, tower_lon, description
      ) AS b
    ) AS src
    WHERE a_tower != b_tower
    AND dist_km <= {max_cluster_width} 
    """.format(tower_pair_dets=conf.TOWER_PAIR_DETS,
        feed_rtis=conf.FEED_RTIS_LOCAL,
        min_lat=conf.NZ_BB_MIN_LAT, max_lat=conf.NZ_BB_MAX_LAT,
        min_lon=conf.NZ_BB_MIN_LON, max_lon=conf.NZ_BB_MAX_LON,
        scalar=conf.ST_DISTANCE2KM_SCALAR, srid=conf.WGS84_SRID,
        max_cluster_width=MAX_CLUSTER_WIDTH)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    print("Finished making '{}'".format(conf.TOWER_PAIR_DETS))

def update_tower_cluster(pair_dets, tower, tower_cluster):
    """
    Clusters are all towers linked together by links no longer than
    MAX_IMMEDIATE_NEIGHBOUR_DIST with a maximum gap between any of them of
    MAX_CLUSTER_WIDTH. That check happens at the end for each new cluster.

    If we've already got this tower in the cluster no point processing it again.

    More efficient if mutating tower_cluster as we go so subsequent steps
    already know about towers captured and don't process them pointlessly again.

    NOTE - mustn't add tower to tower_cluster before rerunning
    update_tower_cluster or it will consider the tower to already be in cluster
    and thus requiring no further processing.
    """
    if tower in tower_cluster: return  ## already processed
    tower_cluster.add(tower)
    for pair_key, dist_km in pair_dets.items():
        if tower not in pair_key: continue  ## not relevant pair
        if dist_km <= MAX_IMMEDIATE_NEIGHBOUR_DIST:  ## append tower if close enough
            new_tower = list(pair_key - set([tower, ]))[0]
            update_tower_cluster(pair_dets, new_tower, tower_cluster)

def check_clusters(tower_pair_dets, clusters, unclustered_tower_n):
    """
    The maximum gap between any tower in a cluster is MAX_CLUSTER_WIDTH. We can
    tell if a pair is too far apart because it will will not be a key in
    tower_pair_dets. Note -- if a-b is missing so will b-a so no need to do both
    permutations - unique combinations are enough.

    Also report on the number of towers in all clusters - should equal the
    original number of unclustered towers.
    """
    for cluster in clusters:
        all_tower_pairs = combinations(cluster, 2)
        for tower_pair in all_tower_pairs:
            if frozenset(tower_pair) not in tower_pair_dets.keys():  ## Must be more than MAX_CLUSTER_WIDTH apart given that tower_pair_dets includes everything within maximum
                raise Exception("Towers {} and {} are in same cluster but are "
                    "more than {} km apart - problem in clustering".format(
                    *list(tower_pair), MAX_CLUSTER_WIDTH))
    towers = []
    for cluster in clusters:
        towers.extend(cluster)
    clustered_tower_n = len(towers)
    if clustered_tower_n != unclustered_tower_n:
        raise Exception("Something went wrong assigning towers to clusters - "
            "mismatch in tower numbers between {:,} (unclustered) and {:,} "
            "(clustered)".format(unclustered_tower_n, clustered_tower_n)) 


class TestCoords(unittest.TestCase):
    """
    cd ~/Documents/tourism/storage/personas && nosetests voronai_demo.py
    """

    def test__get_avg_dps(self):
        tests = [
            ((-36.12345, 171.686), 4),
            ((-36.123, 171.686), 3),
            ((-36.12345, 171.6867), 4.5),
            ((-36., 171.), 0),
            ((-36, 171), 0),
            ((-36.1, 171.), 0.5),
        ]
        for coord, output in tests:
            self.assertEqual(_get_avg_dps(coord), output)


def _get_avg_dps(coord):
    lat, lon = coord
    lat_dps = str(lat).partition('.')[2].rstrip('0')
    lon_dps = str(lon).partition('.')[2].rstrip('0')
    avg_dps = (len(lat_dps) + len(lon_dps))/2
    return avg_dps

def get_tower_coord(cur_local, towers):
    """
    For a bunch of supposedly separate towers that are really close together,
    and are almost certainly the same physical tower in reality, get the
    coordinates for each and choose the best (of those with the most precision,
    the first one).
    """
    towers_clause = db.Pg.strs2clause(towers)
    sql = """\
    SELECT DISTINCT tower_lat, tower_lon
    FROM {feed_rtis}
    WHERE tower IN {towers_clause}
    AND tower_lat BETWEEN {min_lat} AND {max_lat}
    AND tower_lon BETWEEN  {min_lon} AND {max_lon}
    ORDER BY tower_lat, tower_lon -- ordered so a determinate result given stopping at first
    """.format(feed_rtis=conf.FEED_RTIS_LOCAL, towers_clause=towers_clause,
        min_lat=conf.NZ_BB_MIN_LAT, max_lat=conf.NZ_BB_MAX_LAT,
        min_lon=conf.NZ_BB_MIN_LON, max_lon=conf.NZ_BB_MAX_LON)
    cur_local.execute(sql)
    coords = cur_local.fetchall()
    coord2use = None
    best_avg_dps = 0
    for coord in coords:
        avg_dps = _get_avg_dps(coord)
        if avg_dps > best_avg_dps:
            coord2use = coord
            best_avg_dps = avg_dps
    return coord2use

def get_tower_rtis(cur_local, towers):
    towers_clause = db.Pg.strs2clause(towers)
    sql = """\
    SELECT DISTINCT cell_id_rti
    FROM {feed_rtis}
    WHERE tower IN {towers_clause}
    ORDER BY cell_id_rti
    """.format(feed_rtis=conf.FEED_RTIS_LOCAL, towers_clause=towers_clause)
    cur_local.execute(sql)
    rtis = [row['cell_id_rti'] for row in cur_local.fetchall()]
    return rtis

def get_towers_dets(con_local, cur_local, make_fresh_source=True):
    """
    Towers (assuming we have one row per cell_id_rti sorted by lat/lon) always
    have one coordinate in the FEED. But two towers can actually be almost the
    same place and, often, almost the same tower id e.g.
    AWSXU    -36.8539,       174.757
    AWSUXW   -36.85389244,   174.7568793
    (both at Union St offramp near Nelson St, Auckland)

    So ... we need to link cell_id_rti's to single tower coordinates not to
    towers. Two "Towers" can actually be the same. Daniel recommended against
    anything which assumed we could aggregate reliably on the first 4 characters
    e.g. AWSX as this could "just break" at some random point in the future. It
    is not guaranteed to work. Actually, it seems to fail now ;-)

    So ... look at using tower to link all cell_id_rti's to a single location
    that sensibly represents the tower location. Can then generate voronoi areas
    for each tower cluster (all too close to each other to be separate towers).
    Person type data can also be assigned to each tower cluster and thus to each
    cell_id_rti for that day.
    """
    debug = False
    ## get close-together towers
    if make_fresh_source:
        _make_pair_dets_tbl(con_local, cur_local)
    sql_tower_pairs = """\
    SELECT * FROM {tower_pair_dets}
    ORDER BY a_tower, b_tower
    """.format(tower_pair_dets=conf.TOWER_PAIR_DETS)
    cur_local.execute(sql_tower_pairs)
    data = cur_local.fetchall()
    ## A) Cluster towers *******************************************************
    tower_pair_dets = {}
    for a_tower, b_tower, dist_km in data:
        tower_pair_dets[frozenset([a_tower, b_tower])] = dist_km
    already_handled_towers = set()  ## a tower may have been handled when looking at neighbours of a previous tower - no point looking at again given will create same cluster.
    clusters = []
    for a_tower, unused_b_tower, unused_dist_km in data:
        if a_tower in already_handled_towers:
            continue
        tower_cluster = set()
        update_tower_cluster(tower_pair_dets, a_tower, tower_cluster)
        clusters.append(sorted(list(tower_cluster)))
        already_handled_towers = already_handled_towers.union(tower_cluster)
    unclustered_tower_n = len(set([a_tower
        for a_tower, unused_b_tower, unused_dist_km in data]))
    check_clusters(tower_pair_dets, clusters, unclustered_tower_n)
    ## create tower_dets
    towers_dets = []
    for multi_tower_cluster_i, cluster in enumerate(clusters):
        if debug: print(cluster)
        tower_dets = {
            'tower_idx': multi_tower_cluster_i,
            'tower_coord': get_tower_coord(cur_local, towers=cluster),
            'towers': cluster,
            'rtis': get_tower_rtis(cur_local, towers=cluster),
        }
        towers_dets.append(tower_dets)
    ## B) Remaining (more isolated, unclustered) towers ************************
    sql_all_towers = """\
    SELECT DISTINCT ON (tower)
    tower, tower_lat, tower_lon
    FROM {feed_rtis}
    WHERE tower_lat BETWEEN {min_lat} AND {max_lat}
    AND tower_lon BETWEEN  {min_lon} AND {max_lon}
    ORDER BY tower, tower_lat, tower_lon, description
    """.format(feed_rtis=conf.FEED_RTIS_LOCAL,
        min_lat=conf.NZ_BB_MIN_LAT, max_lat=conf.NZ_BB_MAX_LAT,
        min_lon=conf.NZ_BB_MIN_LON, max_lon=conf.NZ_BB_MAX_LON)
    cur_local.execute(sql_all_towers)
    start_i = multi_tower_cluster_i + 1
    unclustered_towers = [(tower, tower_lat, tower_lon)
        for tower, tower_lat, tower_lon in cur_local.fetchall()
        if tower not in already_handled_towers]
    for single_tower_cluster_i, (tower, tower_lat, tower_lon) in enumerate(unclustered_towers, start_i):
        towers = [tower, ]
        tower_dets = {
            'tower_idx': single_tower_cluster_i,
            'tower_coord': (tower_lat, tower_lon),
            'towers': towers,
            'rtis': get_tower_rtis(cur_local, towers),
        }
        towers_dets.append(tower_dets)
    return towers_dets

def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite voronoi regions in a 2D diagram to finite
    regions.

    From https://stackoverflow.com/questions/20515554/...
    ...colorize-voronoi-diagram/20678647#20678647

    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.

    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.

    """
    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())

    return new_regions, np.asarray(new_vertices)

def get_tower_coverage_polygons(cur_local, points, points_rtis,
        display_mpl_map=True, colourise_mpl_map=True, display_folium_map=False):
    debug = False
    test_map = False
    vor = Voronoi(np.array(points))
    ## close open regions
    regions, vertices = voronoi_finite_polygons_2d(vor)
    ## trim regions to coastline - https://stackoverflow.com/questions/34968838/python-finite-boundary-voronoi-cells
    polygon_points = [(lon, lat) for lat, lon in NZ_POLYGON_COORDS_ROUGH]
    mask = Polygon(np.array(polygon_points))
    new_vertices = []

    def gen_colours():
        for area_colour, popup_colour in cycle(conf.FOLIUM_COLOURS):
            yield area_colour, popup_colour

    colours = gen_colours()

    for region in regions:
        polygon = vertices[region]
        shape = list(polygon.shape)
        shape[0] += 1
        p = (Polygon(np.append(polygon, polygon[0]).reshape(*shape))
            .intersection(mask))
        try:
            poly = np.array(list(zip(p.boundary.coords.xy[0][:-1],
                p.boundary.coords.xy[1][:-1])))
        except NotImplementedError:
            print("The intersection of one of the Voronoi regions and the crude"
                " polygon defining NZ coast resulted in a multipolygon. We "
                "actually only want one of the polygons in that case - namely "
                "the one with the tower point inside it. Not implemented (yet) "
                "because this problem doesn't happen when realistic tower "
                "locations. Example of how this could happen in code as "
                "ASCII art ;-) - Stewart Island provides example of how it "
                "could happen as the NZ boundary goes in and back out and a "
                "Southland Voronoi region could stretch out and cross the gap.")
            """ Thanks to http://picascii.com/

            @@  Clipping             @@@@@@@  South Island .....................                         
            @@  Boundary             @@@@@@@ Boundary ..........................                              
            @@                      @@@@@@@  ...................................                                       
           @@                     @@@@@@@ ......................................                                          
           @@                    @@@@@@@  .......... SOUTHLAND .................                     
          @@                @@@@@@@@@@@@@@@ ....................................                                        
         @@                @@::@@@@@@@@:::@@@@@ ................................                                    
         @@      water   .@@:::@@@@@@@:::::::@@@  ..............................                                  
         @@              @@:::@@@@@@@@::::::::::@@@ ............................  
         @@             @@::::@@@@@@:::::::::::::::@@@..........................    
        @@            @@::::@@@@@@::::::::::::::::::::@@ ....................... 
       @@            @@:::@@@@@@@::::::::::::::::::::::@@ ......................    
       @@            @@::@@@@@@@@::::::::::::::::::::::::@@ ....................    
      @@            @@::@@@@@@@:::::::::::::::::::::::::@@ ....................     
      @@           @@:::@@@@@@::::::::::::::::::::::::::@@ <-- YES we want this       
      @@           @@::@@@@@@@:::::::::::::::::::::::::@@@ shaded polygon           
      @@          @@::@@@@@@@::::::::::::::::::::::::::@@ ......................    
     @@           @@::@@@@@@@::::::::::X::::::::::::::@@ .......................    
     @@          @@:::@@@@@@@::::::::TOWER:::::::::::@@@ .......................    
     @@        @@@::::::@@@@@@@@::::::::::::::::::::@@ .........................
     @@       @@::::::::::@@@@@@@::::::::::::::::::@@@ .........................
     @@      @@::::::::::::@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
      @@    @@::::::::::::::@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
       @@   @@:::::::::::::::::@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         @@ @@:::::::::::::::::::::::::::::::::::::@@                               
          @@@@@@:::::::::::::::::::::::::::::::::::@@                               
            @@  @@@@@@::::::::::::::::::::::::::::@@           Foveaux Strait      
            @@       @@@@@::::::::::::::::::::::::@@            (water)             
           @@            @@@@@@@::::::::::::::::::@@                                  
          @@                   @@@@@@@@@@@@@@@@@@@@@                                
          @@                                     @@@@@@@@@@@@                       
    Raw  @@                                     @@          @@                     
 Voronoi @@                                     @@          @@                     
  polygon  @@                                   @@        @@@                       
 boundary   @@                                  @@     @@@                          
            @@                @@@@@@@@@@@@@@@@@@@@@@@@@@                            
            @@@             @@@:::::::::::::::: @@                                  
             @@@          @@@:::::::::::::::::::@@                                  
              @@         @@::::::::::::::::::::@@                                   
                @@       @@:::::::::::::::::::@@                                    
                @@       @@::::::::::::::::::@@                                    
                @@       @@::Oops - don't :::@@          @@@@@@@@@@@@@@@            
                 @@      @@::want this area :@@       @@@@@@@@@@@@@@@@@@@@@@@@@@    
                  @@     @@:::::::::::::::::@@   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                   @@    @@.:::::::::::::::@@   @@@@@@@@@@@.............. @@@@@@
                   @@    @@@ :::::::::::::@@   @@@@@@@@@@ ................... @@
                    @@@   @@ :::::::::::@@     @@@@@@@ .........................   
                      @@@@@@@@@@@@@@@@@@       @@@@@@ ..........................    
                           @@@                 @@@@@@ Stewart ..................    
                            @@@                @@@@@@  Island ..................    
                             @@                @@@@@@  Boundary ................    
                             @@@               @@@@@@@ .........................    
                              @@               @@@@@@@ .........................    
                              @@               @@@@@@@ ....... STEWART .........           
                               @@               @@@@@@@ ....... ISLAND .........          
            """
            print(p)
            print("Leaving polygon unclipped - expect misshapen areas")
            p = Polygon(np.append(polygon, polygon[0]).reshape(*shape))
            poly = np.array(list(zip(p.boundary.coords.xy[0][:-1],
                p.boundary.coords.xy[1][:-1])))
        new_vertices.append(poly)
        if display_mpl_map and colourise_mpl_map:
            plt.fill(*zip(*poly), alpha=0.4)
    if debug:
        for i in [0, 50, 300, 1000]:
            print(points[i])
            print(new_vertices[i])
    if display_mpl_map:
        plt.plot(points[:,0], points[:,1], 'ko')
        plt.title("Clipped Voronois")
        plt.show()
    if display_folium_map:
        assert(len(points) == len(new_vertices))
        title = "tower coverage areas"
        geoms_dets = []
        i = 0
        for point, rtis, vertices in zip(points, points_rtis, new_vertices):
            lbl_rtis = ', '.join(str(rti) for rti in rtis)
            lbl_coord = (point[1], point[0])  ## swap lon/lat
            polygon = [(lat, lon) for lon, lat in vertices]
            polygon.append(polygon[0])
            geom = db.Pg.geom_from_coords(cur_local, polygon,
                srid=conf.WGS84_SRID)
            geojson = db.Pg.geojson_from_geom(cur_local, geom)
            area_colour, popup_colour = next(colours)
            area_opacity = 0.3
            geoms_dets.append(GeomDets(geojson, area_colour, area_opacity,
                lbl_rtis, lbl_coord, popup_colour))
            i += 1
            if test_map and i > 20:
                break
        map_url = folium_map.FoliumMap.geoms_dets_to_chart(title, geoms_dets,
            use_popup_not_lbl=True, zoom_start=8, show_border=True,
            show_points=False)
        open_new_tab(map_url)
    tower_coverage_polygons = dict(zip(points, new_vertices))
    return tower_coverage_polygons

def _get_tower_points(cur_local):
    """
    Get tower points ready for plotting.

    TODO - use actual tower coordinates by mapping from cell_id_rti's to
    cleaned-up tower coordinates.
    """
    sql = """\
    SELECT
      lon,
      lat,
        ARRAY_AGG(cell_id_rti ORDER BY cell_id_rti) AS
      rtis
    FROM (
      SELECT DISTINCT ON (cell_id_rti)
          tower_lat AS
        lat,
          tower_lon AS
        lon,
        cell_id_rti
      FROM {feed_rtis}
      WHERE tower_lat BETWEEN {min_lat} AND {max_lat}
      AND tower_lon BETWEEN  {min_lon} AND {max_lon}
      ORDER BY cell_id_rti, tower_lat, tower_lon, description
    ) AS current_src
    GROUP BY lat, lon
    ORDER BY lat, lon
    """.format(feed_rtis=conf.FEED_RTIS_LOCAL,
        min_lat=conf.NZ_BB_MIN_LAT, max_lat=conf.NZ_BB_MAX_LAT,
        min_lon=conf.NZ_BB_MIN_LON, max_lon=conf.NZ_BB_MAX_LON)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    points = np.array([(lon, lat) for lon, lat, unused_rtis in data])
    points_rtis = [rtis for unused_lon, unused_lat, rtis in data]
    return points, points_rtis


def main():
    con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    towers_dets = get_towers_dets(con_local, cur_local, make_fresh_source=False)
    points = []
    points_rtis = []
    for tower_dets in towers_dets:
        lat, lon = tower_dets['tower_coord']
        points.append((lon, lat))
        points_rtis.append(tower_dets['rtis'])
    tower_coverage_polygons = get_tower_coverage_polygons(
        cur_local,
        points, points_rtis,
        display_mpl_map=False, colourise_mpl_map=True, display_folium_map=True)

if __name__ == '__main__':
    main()

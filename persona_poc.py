#! /usr/bin/env python3

from collections import defaultdict
import datetime
from itertools import count
import json
from pprint import pprint as pp
from random import randint, sample

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
from calendar import week
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
new_path = os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT))
sys.path.insert(0, new_path)

from voyager_shared import db

GRID12S_TBL = 'grid12s'
GRID12_METADATA_TBL = 'grid12_metadata'
PEOPLE_TYPES_TBL = 'people_types'
HOME_GRID12_TBL = 'home_grid12s'
SEEN_IN_DAY_TBL = 'seen_in_day'
DEST_HOMES_TBL = 'dest_homes'

QUEENSTOWN_GRID12 = 5

WEEK_DATES = ['2017-09-04', '2017-09-05', '2017-09-06', '2017-09-07',
    '2017-09-08', '2017-09-09', '2017-09-10', ]

def make_home_grid12s(con_local, cur_local):
    """
    Note - all data is fake - real places have been used to make it easier to
    assess face-validity.

    For simplicity, only have four home origin grid12's (not actually grid
    shapes but actual political boundaries - easier to get from QGIS and more
    intuitive besides):
    1: Remuera
    2: Mt Eden
    3: Mt Roskill
    4: Clendon

    and one destination grid12:

    5: Queenstown

    For simplicity, only look at one week.

    For simplicity, make IMSI's start with the digit of their home grid square.
    E.g. 1021 lives in grid 1.

    Only have 5 social groups:
    1: Old Money
    2: Professional
    3: Lower Middle
    4: Working Class
    5: Benefits

    For simplicity, carved up all sub-"grid12"s into social class areas. Easy to
    check getting expected results with reference to a map in QGIS where the
    "grid12"s can be overlayed.
    """
    db.Pg.drop_tbl(con_local, cur_local, tbl=HOME_GRID12_TBL)
    sql_make_tbl = """\
    CREATE TABLE {home_grid12s} (
      id serial,
      week_starting text,
      imsi int,
      grid12 int,
      PRIMARY KEY(id)
    )
    """.format(home_grid12s=HOME_GRID12_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    sql_insert_start = """\
    INSERT INTO {home_grid12s}
    (week_starting, imsi, grid12)
    VALUES
    """.format(home_grid12s=HOME_GRID12_TBL)
    imsis = []
    for grid12 in (1, 2, 3, 4):
        val_clauses = []
        for n in range(50):
            imsi = (100*grid12) + n
            imsis.append(imsi)
            week_starting = WEEK_DATES[0]
            val_clauses.append("('{}', {}, {})".format(week_starting, imsi,
                grid12))
        sql_insert = sql_insert_start + ", ".join(val_clauses)
        cur_local.execute(sql_insert)
        con_local.commit()
    return imsis

def make_seen_in_day_tbl(con_local, cur_local):
    """
    For each home region, randomly select a group to be tourists to Queenstown.
    Have different proportions traveling depending on socio-economic status.
    Assume richer people travel more. 
    """
    db.Pg.drop_tbl(con_local, cur_local, tbl=SEEN_IN_DAY_TBL)
    sql_make_tbl = """\
    CREATE TABLE {seen_in_day} (
      id serial,
      date text,
      imsi int,
      grid12 int,
      PRIMARY KEY (id)
    )
    """.format(seen_in_day=SEEN_IN_DAY_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    sql_insert_start = """\
    INSERT INTO {seen_in_day}
    (date, imsi, grid12)
    VALUES
    """.format(seen_in_day=SEEN_IN_DAY_TBL)
    sql_imsis_tpl = """\
    SELECT imsi
    FROM {home_grid12s}
    WHERE grid12 = %s
    ORDER BY imsi
    """.format(home_grid12s=HOME_GRID12_TBL)
    sql_grid12_ids = """\
    SELECT id
    FROM {grid12s}
    ORDER BY id
    """.format(grid12s=GRID12S_TBL)
    cur_local.execute(sql_grid12_ids)
    grid12_ids = [row['id'] for row in cur_local.fetchall()]
    for grid12_id in grid12_ids:
        val_clauses = []
        cur_local.execute(sql_imsis_tpl, (grid12_id, ))
        imsis = [row['imsi'] for row in cur_local.fetchall()]
        imsi_sample = sample(imsis, int(50/(grid12_id*1.5)))  ## fewer tourists to Queenstown as larger grid12 id
        for imsi in imsi_sample:
            for week_date in WEEK_DATES[randint(0, 4): randint(4, 7)]:  ## varying lengths of stay in Queenstown
                val_clauses.append("('{}', {}, {})".format(week_date, imsi,
                    QUEENSTOWN_GRID12))
        sql_insert = sql_insert_start + ", ".join(val_clauses)  ## given small number of imsi's per grid12 safe to build single insert statements at this point
        cur_local.execute(sql_insert)
        con_local.commit()

def make_dests_homes_tbl(con_local, cur_local):
    """
    Make rows based on Seen in Day where we have week_started (always the same
    in PoC), destination (always Queenstown in PoC), IMSI, and home grid12.
    """
    debug = False
    CHUNK = 100
    db.Pg.drop_tbl(con_local, cur_local, tbl=DEST_HOMES_TBL)
    sql_make_tbl = """\
    CREATE TABLE {dest_homes} (
      id serial,
      date text,
      imsi int,
      dest_grid12 int,
      home_grid12 int,
      PRIMARY KEY (id)
    )
    """.format(dest_homes=DEST_HOMES_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    sql_insert_start = """\
    INSERT INTO {dest_homes}
    (date, imsi, dest_grid12, home_grid12)
    VALUES
    """.format(dest_homes=DEST_HOMES_TBL)
    sql_values = """\
    SELECT
      src.date,
      src.imsi,
      src.dest,
      home.grid12
    FROM (
      SELECT
        seen.date,
        week_starting,
        seen.imsi,
          {queenstown_grid12} AS
        dest
      FROM {seen_in_day} AS seen INNER JOIN (
        SELECT week_starting::text, DATE(date)::text
        FROM (
        SELECT
            '{week_starting}' AS
          week_starting,
            generate_series(
              '{week_starting}',
              '{week_starting}'::date + interval '6 days',
              interval '1 day') AS
          date
        ) AS dates2weeks_raw
      ) AS dates2weeks
      USING(date)
    ) AS src
    INNER JOIN {home_grid12s} AS home
    USING(imsi, week_starting)
    """.format(queenstown_grid12=QUEENSTOWN_GRID12, seen_in_day=SEEN_IN_DAY_TBL,
        home_grid12s=HOME_GRID12_TBL, week_starting=WEEK_DATES[0])
    cur_local.execute(sql_values)
    data = cur_local.fetchall()
    if debug:
        print(len(data))
        print(str(cur_local.query, 'utf-8'))
        for row in data:
            print(row)
    for i in count():
        data2use = data[i*CHUNK:(i+1)*CHUNK]
        if not data2use:
            break
        sql_insert = (sql_insert_start
            + ", ".join("('{}', {}, {}, {})".format(*row) for row in data2use))
        cur_local.execute(sql_insert)
        con_local.commit()

def make_grid12s(con_local, cur_local):
    """
    Bring all geoms together along with labels so we can create grid12 metadata.
    """
    db.Pg.drop_tbl(con_local, cur_local, tbl=GRID12S_TBL)
    sql_make_tbl = """\
    CREATE TABLE {grid12s} AS
    SELECT row_number() OVER () AS
      id,
      lbl,
      geom
    FROM (
      SELECT 'Remuera' AS lbl, geom
      FROM remuera
      UNION ALL
      SELECT 'Mt Eden' AS lbl, geom
      FROM mt_eden
      UNION ALL
      SELECT 'Mt Roskill' AS lbl, geom
      FROM mt_roskill
      UNION ALL
      SELECT 'Clendon' AS lbl, geom
      FROM clendon
    ) AS src
    """.format(grid12s=GRID12S_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    print("Finished making {grid12s}".format(grid12s=GRID12S_TBL))

def make_people_types_tbl(con_local, cur_local):
    """
    Bring all geoms together along with labels so we can create grid12 metadata. 
    """
    db.Pg.drop_tbl(con_local, cur_local, tbl=PEOPLE_TYPES_TBL)
    sql_make_tbl = """\
    CREATE TABLE {people_types} AS
    SELECT row_number() OVER () AS
      id,
      lbl,
      geom
    FROM (
      SELECT 'Old Money' AS lbl, geom
      FROM old_money
      UNION ALL
      SELECT 'Professionals' AS lbl, geom
      FROM professionals
      UNION ALL
      SELECT 'Lower Middle' AS lbl, geom
      FROM lower_middle
      UNION ALL
      SELECT 'Working Class' AS lbl, geom
      FROM working_class
      UNION ALL
      SELECT 'Benefits' AS lbl, geom
      FROM benefits
    ) AS src
    """.format(people_types=PEOPLE_TYPES_TBL)
    cur_local.execute(sql_make_tbl)
    con_local.commit()
    print("Finished making {people_types}".format(
        people_types=PEOPLE_TYPES_TBL))

def make_grid12_people_metadata(con_local, cur_local):
    """
    For each "grid12" e.g. Mt Eden, make and store metadata on people types
    within that "grid12".

    Get values out using syntax like:

    SELECT
      id,
        metadata->'people_type'->'Professionals' AS
      pros
    FROM grid12_metadata;
    """
    debug = True
    sql_overlap = """\
    SELECT
        grid12s.id AS
      grid12_id,
        grid12s.lbl AS
      grid12_lbl,
        people_types.lbl AS
      people_type_lbl,
        (
          ST_Area(ST_Intersection(grid12s.geom, people_types.geom))
          /
          ST_Area(grid12s.geom)
        )::double precision AS
      fraction_overlap
    FROM {grid12s} AS grid12s
    CROSS JOIN
    {people_types} AS people_types
    """.format(grid12s=GRID12S_TBL, people_types=PEOPLE_TYPES_TBL)
    cur_local.execute(sql_overlap)
    area_overlaps = cur_local.fetchall()
    grid12_metadata_init = defaultdict(dict)
    for (grid12_id, grid12_lbl, people_type_lbl,
            fraction_overlap) in area_overlaps:
        if debug:
            print(grid12_id, grid12_lbl, people_type_lbl, fraction_overlap)
        grid12_metadata_init[grid12_id][people_type_lbl] = fraction_overlap
    grid12_metadata = {}
    for grid12_id, metadict in grid12_metadata_init.items():
        grid12_metadata[grid12_id] = {'people_type': metadict}
    if debug: pp(grid12_metadata)
    db.Pg.drop_tbl(con_local, cur_local, tbl=GRID12_METADATA_TBL)
    sql_make_table = """\
    CREATE TABLE {grid12_metadata} (
      id int,
      metadata json,
      PRIMARY KEY (id)
    )
    """.format(grid12_metadata=GRID12_METADATA_TBL)
    cur_local.execute(sql_make_table)
    con_local.commit()
    sql_insert_start = """\
    INSERT INTO {grid12_metadata}
    (id, metadata)
    VALUES
    """.format(grid12_metadata=GRID12_METADATA_TBL)
    sql_insert = (sql_insert_start
        + ", ".join("({}, ('{}')::json)".format(grid12_id, json.dumps(metadata))
        for grid12_id, metadata in grid12_metadata.items()))
    cur_local.execute(sql_insert)
    con_local.commit()
    print("Finished making {grid12_metadata}".format(
        grid12_metadata=GRID12_METADATA_TBL))

def make_src(con_local, cur_local):
    make_grid12s(con_local, cur_local)
    make_people_types_tbl(con_local, cur_local)
    make_home_grid12s(con_local, cur_local)
    make_seen_in_day_tbl(con_local, cur_local)
    make_dests_homes_tbl(con_local, cur_local)  ## like arrivals and visits
    make_grid12_people_metadata(con_local, cur_local)

def generate_insight(cur_local):
    """
    For each row where home "grid12" not same as dest "grid12" (none in PoC will
    be because Queenstown is not an origin) grab metadata for home grid12 and
    add each part to appropriate subtotal e.g. "Old Money" total.
    """
    sql = """\
    SELECT
      date,
      dest_grid12,
      people_type,
        ROUND(SUM(fraction), 2)::float AS
      n_people
    FROM (
        SELECT date, dest_grid12, home_grid12, 'Old Money' AS people_type, (metadata->'people_type'->>'Old Money')::numeric AS fraction
        FROM {dest_homes} AS dests
        INNER JOIN
        {grid12_metadata} AS meta
        ON dests.home_grid12 = meta.id
        UNION ALL
        SELECT date, dest_grid12, home_grid12, 'Professionals', (metadata->'people_type'->>'Professionals')::numeric
        FROM {dest_homes} AS dests
        INNER JOIN
        {grid12_metadata} AS meta
        ON dests.home_grid12 = meta.id
        UNION ALL
        SELECT date, dest_grid12, home_grid12, 'Lower Middle', (metadata->'people_type'->>'Lower Middle')::numeric
        FROM {dest_homes} AS dests
        INNER JOIN
        {grid12_metadata} AS meta
        ON dests.home_grid12 = meta.id
        UNION ALL
        SELECT date, dest_grid12, home_grid12, 'Working Class', (metadata->'people_type'->>'Working Class')::numeric
        FROM {dest_homes} AS dests
        INNER JOIN
        {grid12_metadata} AS meta
        ON dests.home_grid12 = meta.id
        UNION ALL
        SELECT date, dest_grid12, home_grid12, 'Benefits', (metadata->'people_type'->>'Benefits')::numeric
        FROM {dest_homes} AS dests
        INNER JOIN
        {grid12_metadata} AS meta
        ON dests.home_grid12 = meta.id
    ) AS src
    WHERE dest_grid12 != home_grid12  -- if stayed home, not a visitor ;-)
    GROUP BY date, dest_grid12, people_type
    ORDER BY date, dest_grid12, people_type
    """.format(dest_homes=DEST_HOMES_TBL, grid12_metadata=GRID12_METADATA_TBL)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    return data

def audit_indiv_result(cur_local, date, dest_region_grid12, people_type):
    """
    E.g. if on 2017-09-07 we have 24.58 'Professionals' going to Queenstown
    (id 5) show every imsi that contributed, their home_grid12, and the
    breakdown for that grid12 by people_type. Then view the map and check it is
    sane.
    """
    debug = True
    week_starting = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    while True:
        if week_starting.strftime("%A") == 'Monday':
            break
        else:
            week_starting = week_starting - datetime.timedelta(days=1)
    week_starting
    ## There are sql ways of doing this (which I use elsewhere) but I want something more transparent given this is an audit
    sql = """\
    SELECT
      imsi,
      home_grid12,
        meta.metadata->'people_type' AS
      people_type,
        (meta.metadata->'people_type'->>'{people_type}')::float AS
      fraction
    FROM (
      SELECT
        seen.imsi,
          get_home_grid12.grid12 AS
        home_grid12
      FROM {seen_in_day} AS seen
      INNER JOIN
      (
        SELECT *
        FROM {home_grid12s}
        WHERE week_starting = '{week_starting}'
      ) AS get_home_grid12
      USING (imsi)
      WHERE date = '{date}'
      AND seen.grid12 = {dest_region_grid12}
    ) AS src
    INNER JOIN
    {grid12_metadata} AS meta
    ON src.home_grid12 = meta.id
    WHERE (meta.metadata->'people_type'->>'{people_type}')::numeric > 0
    """.format(date=date, seen_in_day=SEEN_IN_DAY_TBL,
        home_grid12s=HOME_GRID12_TBL, week_starting=week_starting,
        dest_region_grid12=dest_region_grid12,
        grid12_metadata=GRID12_METADATA_TBL, people_type=people_type)
    cur_local.execute(sql)
    if debug: print(str(cur_local.query, 'utf-8'))
    data = cur_local.fetchall()
    fraction_tot = 0
    for row in data:
        print(row)
        fraction_tot += row['fraction']
    print(len(data), fraction_tot)

def main():
    make_fresh_src = False

    con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    if make_fresh_src:
        make_src(con_local, cur_local)
    data = generate_insight(cur_local)
    for row in data:
        print(row)
    audit_indiv_result(cur_local, date='2017-09-07',
        dest_region_grid12=QUEENSTOWN_GRID12, people_type='Professionals')

if __name__ == '__main__':
    main()
